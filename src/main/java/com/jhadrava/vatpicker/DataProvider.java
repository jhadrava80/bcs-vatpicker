package com.jhadrava.vatpicker;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class DataProvider {

    private String sourceUrl;

    public DataProvider(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public InputStream getInputStream() throws IOException {
        return new URL(sourceUrl).openStream();
    }

}
