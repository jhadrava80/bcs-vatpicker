package com.jhadrava.vatpicker;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jhadrava.vatpicker.datatypes.AllVatData;

import java.io.IOException;
import java.io.InputStream;
import java.util.TimeZone;

/**
 * Implements JSON-to-POJO conversion.
 */
public class DataParser {

    private ObjectMapper mapper;

    public DataParser() {
        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
    }

    /**
     * Converts input stream JSON to POJO.
     * @param stream JSON input
     * @return Parsed data
     * @throws IOException Thrown on malformed input, input too long, and other IO troubles
     */
    public AllVatData parse(InputStream stream) throws IOException {
        return mapper.readValue(stream, AllVatData.class);
    }
}
