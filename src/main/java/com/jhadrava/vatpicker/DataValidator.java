package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.AllVatData;
import com.jhadrava.vatpicker.datatypes.CountryVatData;
import com.jhadrava.vatpicker.datatypes.ValidationException;
import com.jhadrava.vatpicker.datatypes.VatPeriod;

import java.time.LocalDate;

public class DataValidator {
    /**
     * Checks data validity.
     *
     * @param data
     * @throws ValidationException On invalid data
     *
     *
     * TODO Better problem description in exception message
     */
    public void validate(AllVatData data) throws ValidationException {
        for (CountryVatData countryVatData : data.getRates()) {
            if (countryVatData.getName() == null || countryVatData.getCode() == null || countryVatData.getCountryCode() == null) {
                // missing country name or codes
                throw new ValidationException();
            }
            if (countryVatData.getPeriods() == null || countryVatData.getPeriods().isEmpty()) {
                // missing or empty rate list
                throw new ValidationException();
            }
            for (VatPeriod vatPeriod : countryVatData.getPeriods()) {
                if (vatPeriod.getEffectiveFrom() == null || vatPeriod.getEffectiveFrom().isAfter(LocalDate.now())) {
                    // missing or future effective-from
                    throw new ValidationException();
                }
                if (vatPeriod.getRates() == null || vatPeriod.getRates().getStandard() == null) {
                    // missing standard rate
                    throw new ValidationException();
                }
            }
        }
    }

}
