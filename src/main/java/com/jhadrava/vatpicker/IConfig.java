package com.jhadrava.vatpicker;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Interface for config params.
 * Create and use an implementing class for non-default param values.
 */
public interface IConfig {
    /**
     * @return Data source URL
     */
    default String getDataUrl() {
        return "http://jsonvat.com";
    }

    /**
     * @return List of supported values for the field "version" in the input data.
     * As of 2019/01/21 the field seems to contain always the value null.
     */
    default Set<String> getSupportedDataVersions() {
        return new HashSet<>(Arrays.asList((String)null));
    }

    /**
     * @return Number of  highest VAT countries to print out
     */
    default int getHighVatNumber() {
        return 3;
    }

    ;

    /**
     * @return Number of lowest VAT countries to print out
     */
    default int getLowVatNumber() {
        return 3;
    }
}
