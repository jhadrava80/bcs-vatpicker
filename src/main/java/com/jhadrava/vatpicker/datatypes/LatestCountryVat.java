package com.jhadrava.vatpicker.datatypes;

/**
 * A class implementing a pair that consist of (1) a complete country VAT data and (2) its latest VAT period
 */
public class LatestCountryVat {
    private CountryVatData countryVatData;
    private VatPeriod latestVatPeriod;

    public LatestCountryVat(CountryVatData countryVatData, VatPeriod latestVatPeriod) {
        this.countryVatData = countryVatData;
        this.latestVatPeriod = latestVatPeriod;
    }

    public CountryVatData getCountryVatData() {
        return countryVatData;
    }

    public VatPeriod getLatestVatPeriod() {
        return latestVatPeriod;
    }

}
