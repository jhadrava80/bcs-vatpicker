// auto-generated from sample JSON at http://www.jsonschema2pojo.org/
package com.jhadrava.vatpicker.datatypes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import java.time.LocalDate;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VatPeriod {

    @JsonProperty("effective_from")
    private LocalDate effectiveFrom;
    @JsonProperty("rates")
    private VatRates rates;

    @JsonProperty("effective_from")
    public LocalDate getEffectiveFrom() {
        return effectiveFrom;
    }

    @JsonProperty("effective_from")
    public void setEffectiveFrom(LocalDate effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    @JsonProperty("rates")
    public VatRates getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(VatRates rates) {
        this.rates = rates;
    }

}
