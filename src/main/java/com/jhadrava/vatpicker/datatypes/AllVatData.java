// auto-generated from sample JSON at http://www.jsonschema2pojo.org/
package com.jhadrava.vatpicker.datatypes;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AllVatData {

    @JsonProperty("details")
    private String details;
    @JsonProperty("version")
    private Object version;
    @JsonProperty("rates")
    private List<CountryVatData> rates = new ArrayList<CountryVatData>();

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(String details) {
        this.details = details;
    }

    @JsonProperty("version")
    public Object getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Object version) {
        this.version = version;
    }

    @JsonProperty("rates")
    public List<CountryVatData> getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(List<CountryVatData> rates) {
        this.rates = rates;
    }

}
