// auto-generated from sample JSON at http://www.jsonschema2pojo.org/
package com.jhadrava.vatpicker.datatypes;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryVatData {

    @JsonProperty("name")
    private String name;
    @JsonProperty("code")
    private String code;
    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("periods")
    private List<VatPeriod> periods = new ArrayList<VatPeriod>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("country_code")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("periods")
    public List<VatPeriod> getPeriods() {
        return periods;
    }

    @JsonProperty("periods")
    public void setPeriods(List<VatPeriod> periods) {
        this.periods = periods;
    }

}
