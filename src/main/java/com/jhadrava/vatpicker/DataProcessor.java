package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.LatestCountryVat;
import com.jhadrava.vatpicker.datatypes.VatPeriod;
import com.jhadrava.vatpicker.datatypes.CountryVatData;
import com.jhadrava.vatpicker.datatypes.AllVatData;

import java.util.*;

/**
 * This is the main class for processing our input data. Formal description of the input JSON data
 * couldn't be found. Based on the sample JSON data, following assumptions are made:
 * <ul>
 *  <li>
 *      No duplicate country entries in the outer list/array.
 *  </li>
 *  <li>
 *      Each country contains at least one period entry.
 *  </li>
 *  <li>
 *      Each period contains the <i>standard</i> field.
 *  </li>
 *  <li>
 *      Country entries don't contain periods starting in the future.
 *  </li>
 * </ul>
 */
public class DataProcessor {

    private static final PeriodComparator periodComparator = new PeriodComparator();
    private static final LatestStdVatComparator latestStdVatComparator = new LatestStdVatComparator();

    /**
     * Finds latest VAT rates for each country in the input data.
     *
     * @param data Input data
     * @return List of countries with the respective latest VAT rates
     */
    public ArrayList<LatestCountryVat> getLatestRates(AllVatData data) {
        ArrayList<LatestCountryVat> latestVatList = new ArrayList<>(data.getRates().size());
        for (CountryVatData countryVatData : data.getRates()) {
            latestVatList.add(new LatestCountryVat(countryVatData,
                    Collections.max(countryVatData.getPeriods(), periodComparator)));
        }
        return latestVatList;
    }

    /**
     * Sorts country list according to the latest std rate.
     * @param latestVatList List of countries with latest VAT rates
     */
    public void sortByLatestStdRate(List<LatestCountryVat> latestVatList) {
        Collections.sort(latestVatList, latestStdVatComparator);
    }

    /**
     * Compares periods by the effective-from value.
     */
    private static final class PeriodComparator implements Comparator<VatPeriod> {
        @Override
        public int compare(VatPeriod o1, VatPeriod o2) {
            return o1.getEffectiveFrom().compareTo(o2.getEffectiveFrom());
        }
    }

    /**
     * Compares countries by the latest std VAT rate.
     */
    private static final class LatestStdVatComparator implements Comparator<LatestCountryVat> {
        @Override
        public int compare(LatestCountryVat o1, LatestCountryVat o2) {
            return o1.getLatestVatPeriod().getRates().getStandard()
                    .compareTo(o2.getLatestVatPeriod().getRates().getStandard());
        }
    }

}
