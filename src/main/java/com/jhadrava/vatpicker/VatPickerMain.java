package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.AllVatData;
import com.jhadrava.vatpicker.datatypes.LatestCountryVat;
import com.jhadrava.vatpicker.datatypes.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Main class
 */
public class VatPickerMain
{
    private static final Logger logger = LoggerFactory.getLogger(VatPickerMain.class);

    private static final IConfig config = new IConfig(){};

    /**
     * Main method, provides an entry point and glues together all the parts of the program.
     *
     * @param args
     * @throws IOException
     */
    public static void main( String[] args ) throws IOException, ValidationException {
        // read, parse, and validate input data
        try (InputStream inputStream = new DataProvider(config.getDataUrl()).getInputStream()) {
            AllVatData inputData = new DataParser().parse(new BufferedInputStream(inputStream));
            new DataValidator().validate(inputData);

            // sort the data
            DataProcessor dataProcessor = new DataProcessor();
            List<LatestCountryVat> latestRatesList = dataProcessor.getLatestRates(inputData);
            dataProcessor.sortByLatestStdRate(latestRatesList);

            // print out the data
            if (latestRatesList.size() <= config.getLowVatNumber() + config.getHighVatNumber()) {
                // no need for 2 separate sub-lists ~> print out all rates
                latestRatesList.forEach(VatPickerMain::logStdCountryVat);
            } else { // use 2 separate sub-lists
                // print out lowest rates
                latestRatesList.subList(0, config.getLowVatNumber()).stream().forEach(VatPickerMain::logStdCountryVat);
                logger.info("⋮");
                // print out highest rates
                latestRatesList.subList(latestRatesList.size() - config.getHighVatNumber(), latestRatesList.size()).forEach(VatPickerMain::logStdCountryVat);
            }
        }
    }

    private static void logStdCountryVat(LatestCountryVat latestCountryVat) {
        logger.info("{}: {}%", latestCountryVat.getCountryVatData().getName(), latestCountryVat.getLatestVatPeriod().getRates().getStandard());
    }
}
