package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.AllVatData;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class DataParserTest {

    private DataParser dataParser;
    private InputStream fullInputStream;
    private InputStream invalidInputStream;
    private InputStream validInputStream;

    @Before
    public void before() throws IOException {
        dataParser = new DataParser();
        fullInputStream = getClass().getResourceAsStream("/jsonvat-20190121.json");
        validInputStream = getClass().getResourceAsStream("/jsonvat-short.json");
        invalidInputStream = new ByteArrayInputStream("}".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void parseTestFull() throws IOException {
        dataParser.parse(fullInputStream);
    }

    @Test(expected = IOException.class)
    public void parseTestInvalid() throws IOException {
        dataParser.parse(invalidInputStream);
    }

    @Test
    public void parseTestValid() throws IOException {
        AllVatData parsed = dataParser.parse(validInputStream);
        assertEquals(2, parsed.getRates().size());
        assertEquals("PT", parsed.getRates().get(0).getCode());
        assertEquals("AT", parsed.getRates().get(1).getCode());
        assertEquals(2, parsed.getRates().get(1).getPeriods().size());
        assertEquals(LocalDate.of(2016, 1, 1), parsed.getRates().get(1).getPeriods().get(0).getEffectiveFrom());
        assertEquals(LocalDate.of(0, 1, 1), parsed.getRates().get(1).getPeriods().get(1).getEffectiveFrom());
    }

}
