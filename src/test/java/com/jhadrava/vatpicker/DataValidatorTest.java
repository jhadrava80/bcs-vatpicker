package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.AllVatData;
import com.jhadrava.vatpicker.datatypes.CountryVatData;
import com.jhadrava.vatpicker.datatypes.ValidationException;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.*;

public class DataValidatorTest {

    private DataValidator dataValidator;

    @Before
    public void before() {
        dataValidator = new DataValidator();
    }

    @Test(expected = ValidationException.class)
    public void validateTestInvalidCountry() throws ValidationException {
        AllVatData data = new AllVatData();
        data.setRates(Arrays.asList(new CountryVatData()));

        dataValidator.validate(data);
    }

    // TODO more validator tests for various invalid data situations

}