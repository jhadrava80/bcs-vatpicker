package com.jhadrava.vatpicker;

import com.jhadrava.vatpicker.datatypes.*;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;

public class DataProcessorTest {

    private DataProcessor dataProcessor;

    @Before
    public void before() {
        dataProcessor = new DataProcessor();
    }

    @Test
    public void getLatestRatesTest() {
        VatPeriod period2017 = new VatPeriod();
        VatPeriod period2018 = new VatPeriod();
        VatPeriod period2019 = new VatPeriod();
        period2017.setEffectiveFrom(LocalDate.of(2017, 1, 1));
        period2018.setEffectiveFrom(LocalDate.of(2018, 1, 1));
        period2019.setEffectiveFrom(LocalDate.of(2019, 1, 1));

        CountryVatData country = new CountryVatData();
        country.setPeriods(Arrays.asList(period2017, period2019, period2018));

        AllVatData allVatData = new AllVatData();
        allVatData.setRates(Arrays.asList(country));
        List<LatestCountryVat> latestCountryVat = dataProcessor.getLatestRates(allVatData);

        assertSame(period2019, latestCountryVat.get(0).getLatestVatPeriod());
    }

    @Test
    public void sortByLatestStdRateTest() {
        LatestCountryVat latestCountryVat15pct = createLatestCountryVatFromStandardVatPct(15.0);
        LatestCountryVat latestCountryVat20pct = createLatestCountryVatFromStandardVatPct(20.0);
        LatestCountryVat latestCountryVat25pct = createLatestCountryVatFromStandardVatPct(25.0);
        List<LatestCountryVat> latestCountryVatList = Arrays.asList(latestCountryVat25pct, latestCountryVat15pct, latestCountryVat20pct);
        dataProcessor.sortByLatestStdRate(latestCountryVatList);

        assertSame(latestCountryVat15pct, latestCountryVatList.get(0));
        assertSame(latestCountryVat20pct, latestCountryVatList.get(1));
        assertSame(latestCountryVat25pct, latestCountryVatList.get(2));
    }

    private LatestCountryVat createLatestCountryVatFromStandardVatPct(double standardVat) {
        VatPeriod vatPeriod = new VatPeriod();
        vatPeriod.setRates(new VatRates());
        vatPeriod.getRates().setStandard(standardVat);
        return new LatestCountryVat(new CountryVatData(), vatPeriod);
    }

}